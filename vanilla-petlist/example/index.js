import $ from 'jquery';
import TestController from '../src/controllers/TestController';

const petList = document.querySelector('#petList');
const content = document.querySelector('#content');

const GET_REQUEST = 'GET';
const SEARCH_RESULTS_URL = 'http://localhost:3000/static/search.json';
const SEARCH_BOARDING_RESULTS_URL = 'http://localhost:3000/static/search.json?service=boarding';
const SEARCH_SITTING_RESULTS_URL = 'http://localhost:3000/static/search.json?service=sitting';

const MAX_DESCRIPTION_LENGTH = 48;

$(document).ready(function() {
  let testController = new TestController();

  petList.addEventListener('change', filterBoardingResults);

  sendGetRequest(SEARCH_RESULTS_URL, displaySearchResults);
});

function filterBoardingResults(e) {
  // Check delegated event for the checkboxes

  if (e.target.id === 'boardingFilter' && e.target.checked) {
    sendGetRequest(SEARCH_BOARDING_RESULTS_URL, displaySearchResults);
  } else if (e.target.id === 'sittingFilter' && e.target.checked) {
    sendGetRequest(SEARCH_SITTING_RESULTS_URL, displaySearchResults);
  } else if (e.target.id === 'boardingFilter' || e.target.id === 'sittingFilter') {
    // "Unfilter" the results
    sendGetRequest(SEARCH_RESULTS_URL, displaySearchResults);
  }
}


function displaySearchResults(results) {
  // Convert NodeList to array for array functionalities
  const resultsArray = Array.from(results);

  const list = resultsArray.map(result => {
    return generateResultBox(result);
  }).join('');

  content.innerHTML = list;
}

function generateResultBox(result) {
  const title = `<h3><a href='${ getFormattedURL(result.title) }' target='_blank'>${result.title }</a></3>`;
  const ownerName = `<h4>${ getFormattedName(result.user.first, result.user.last) }</h4>`;
  const petName = `<h4>${ result.pet.name }</h4>`;
  const description = `<h4>${ getFormattedDescription(result.description) }</h4>`;

  return (`<div>
    ${ title }
    ${ ownerName }
    ${ petName }
    ${ description }
  </div>`);
}

function getFormattedName(firstName, lastName) {
  return`${ firstName[0].toUpperCase() + firstName.slice(1) } ${ lastName[0].toUpperCase() }.`;
}

function getFormattedDescription(description) {
  if (description.length <= MAX_DESCRIPTION_LENGTH) {
    return description;
  }

  const charAtMaxLength = description[MAX_DESCRIPTION_LENGTH];
  let formattedDescription = description.slice(0, MAX_DESCRIPTION_LENGTH);

  if (charAtMaxLength === ' ') {
    return `${ formattedDescription }...`;
  } else {
    const lastSpace = formattedDescription.lastIndexOf(' ');

    return `${ formattedDescription.slice(0, lastSpace) }...`;
  }
}

function getFormattedURL(title) {
  const formattedWords = title
    .trim()
    .toLowerCase()
    .replace(/[^a-z0-9_-\s]/gi, '') // Remove any character that is not an alphanumeric, underscore, dash, or space character
    .split(' ')
    .filter(word => word !== '');

    return formattedWords.join('-');
}

function sendGetRequest(URL, callback) {
  const XHR = new XMLHttpRequest();

  XHR.onreadystatechange = function onChange() {
    if (XHR.readyState === XMLHttpRequest.DONE) {
      callback(JSON.parse(XHR.response).search);
    }
  }

  XHR.open(GET_REQUEST, URL, true);
  XHR.send(null);
}
