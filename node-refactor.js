'use strict'; // To help enforce best practices

// I am assuming that no other modules will require the functions in this file (it is self-contained)
// As a result, I think it may be best to wrap this in an IIFE to help prevent polluting the global namespace
const refactor = function refactor() {
  var fs = require('fs');

  // Declaring these variables with const because they are not reassigned within this program
  // "army" and "song_11" are removed because they are not used
  const imagine = ['c', 'cmaj7', 'f', 'am', 'dm', 'g', 'e7'];
  const somewhere_over_the_rainbow = ['c', 'em', 'f', 'g', 'am'];
  const tooManyCooks = ['c', 'g', 'f'];
  const iWillFollowYouIntoTheDark = ['f', 'dm', 'bb', 'c', 'a', 'bbm'];
  const babyOneMoreTime = ['cm', 'g', 'bb', 'eb', 'fm', 'ab'];
  const creep = ['g', 'gsus4', 'b', 'bsus4', 'c', 'cmsus4', 'cm6'];
  const paperBag = ['bm7', 'e', 'c', 'g', 'b7', 'f', 'em', 'a', 'cmaj7',
              'em7', 'a7', 'f7', 'b'];
  const toxic = ['cm', 'eb', 'g', 'cdim', 'eb7', 'd7', 'db7', 'ab', 'gmaj7',
          'g7'];
  const bulletproof = ['d#m', 'g#', 'b', 'f#', 'g#m', 'c#'];

  const songs = [];
  const labels = [];
  const allChords = [];
  const labelCounts = [];
  const labelProbabilities = [];
  const chordCountsInLabels = {};
  let probabilityOfChordsInLabels = {}; // This one is declared with let because it is reassigned

  // Moving function executions ("main code") up for easy access
  // This is possible because of function hoisting
  train(imagine, 'easy');
  train(somewhere_over_the_rainbow, 'easy');
  train(tooManyCooks, 'easy');
  train(iWillFollowYouIntoTheDark, 'medium');
  train(babyOneMoreTime, 'medium');
  train(creep, 'medium');
  train(paperBag, 'hard');
  train(toxic, 'hard');
  train(bulletproof, 'hard');

  setLabelProbabilities();
  setChordCountsInLabels();
  setProbabilityOfChordsInLabels();

  classify(['d', 'g', 'e', 'dm']);
  classify(['f#m7', 'a', 'dadd9', 'dmaj7', 'bm', 'bm7', 'd', 'f#m']);

  function train(chords, label){
    songs.push([label, chords]);
    labels.push(label);

    // Change for loop to for of loop for verbosity since the index is not required in this case
    for (const chord of chords) {
      if (!allChords.includes(chord)) {
        allChords.push(chord);
      }
    }

    if(!!(Object.keys(labelCounts).includes(label))){
      // Use the post-increment operator to keep things precise
      labelCounts[label]++;
    } else {
      labelCounts[label] = 1;
    }
  };

  function getNumberOfSongs(){
    return songs.length;
  };

  function setLabelProbabilities(){
    Object.keys(labelCounts).forEach(function(label){
      var numberOfSongs = getNumberOfSongs();
      labelProbabilities[label] = labelCounts[label] / numberOfSongs;
    });
  };

  function setChordCountsInLabels(){
    // Rename 'i' to a more descriptive 'song'
    songs.forEach(function(song){
      // Store song[0] in the more descriptive 'difficulty'
      const difficulty = song[0];

      if(chordCountsInLabels[difficulty] === undefined){
        chordCountsInLabels[difficulty] = {};
      }

      // Store song[1] in the more descriptive 'chords'
      const chords = song[1];

      // Rename 'j' to the more descriptive 'chord'
      chords.forEach(function(chord){
        if(chordCountsInLabels[difficulty][chord] > 0){
          // Use the post-increment operator to keep things precise
          chordCountsInLabels[difficulty][chord]++;
        } else {
          chordCountsInLabels[difficulty][chord] = 1;
        }
      });
    });
  }

  function setProbabilityOfChordsInLabels(){
    probabilityOfChordsInLabels = chordCountsInLabels;

    // Rename 'i' to 'difficulty' and 'j' to 'chord' for readability
    Object.keys(probabilityOfChordsInLabels).forEach(function(difficulty){
      Object.keys(probabilityOfChordsInLabels[difficulty]).forEach(function(chord){
        probabilityOfChordsInLabels[difficulty][chord] =
  probabilityOfChordsInLabels[difficulty][chord] * 1.0 / songs.length;
      });
    });
  }

  function classify(chords){
    var ttal = labelProbabilities;

    console.log(ttal);

    var classified = {};

    // Rename 'obj' to 'difficulty'
    Object.keys(ttal).forEach(function(difficulty){
      // Initializing a constant for the 1.01 value
      const probabilityOffset = 1.01

      var first = labelProbabilities[difficulty] + probabilityOffset;

      chords.forEach(function(chord){
        var probabilityOfChordInLabel =
  probabilityOfChordsInLabels[difficulty][chord];
        if(probabilityOfChordInLabel === undefined){
          first + probabilityOffset;
        } else {
          first = first * (probabilityOfChordInLabel + probabilityOffset);
        }
      });
      classified[difficulty] = first;
    });
    console.log(classified);
  };
}();
